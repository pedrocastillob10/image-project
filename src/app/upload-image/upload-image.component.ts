import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-upload-image',
  templateUrl: './upload-image.component.html',
  styleUrls: ['./upload-image.component.scss']
})
export class UploadImageComponent implements OnInit {
  imagen: any;
  height: any;
  width: any;
  heightInc: any;
  widthInc: any;
  position = '';
  showImagen: boolean = false;
  constructor() { }

  ngOnInit(): void {
  }

  async fileUpload( event: any ) {
    const file = event.target.files;
    this.imagen = file[ 0 ];
    this.preview( file );

  }

  async preview( files: any ) {
    if ( files.length === 0 )
      return;
    var mimeType = files[ 0 ].type;
    if ( mimeType.match( /image\/*/ ) == null ) {
      return;
    }
    var reader = new FileReader();
    reader.readAsDataURL( files[ 0 ] );
    reader.onload = ( e: any ) => {
      this.imagen = { url: reader.result, id: null };
      const image = new Image();
      image.src = e.target.result;
      image.onload = ( rs: any ) => {
        const img_height = rs.currentTarget[ 'height' ];
        const img_width = rs.currentTarget[ 'width' ];
        this.height = img_height;
        this.width = img_width;
        this.showImagen = true;
        this.calculate()
      };
    };
  }

  calculate() {
    const height = 1123;
    const width = 769;
    this.heightInc = this.height;
    this.widthInc = this.width;
    this.position = this.width >= this.height ? 'Horizontal' : 'Vertical';
    switch ( this.position ) {
      case 'Horizontal':
        if ( this.width > height ) {
          const nh = Math.ceil( (this.height / this.width) * height );
          this.width = height;
          if(nh > width){
            this.height = width;
            this.width = Math.ceil( (width/nh) * height );
          }else{
            this.height = nh;
          }
        }else {
          if(this.height > width){
            this.height = width;
            this.width = Math.ceil( (this.height / this.width) * width );
          }
        }
        break;
      case 'Vertical':
        if ( this.height > height ) {
          const nw = Math.ceil( ( this.width/this.height ) * width );
          this.height = height;
          if(nw > width){
            this.width = width;
            this.height = Math.ceil( (width/nw) * width );
          }else{
            this.width = nw;
          }
        }else {
          if(this.width > width){
            this.width = width;
            this.width = Math.ceil( (this.width /this.height ) * width );
          }
        }
        break;
    }
  }
}
